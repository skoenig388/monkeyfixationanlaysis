%% Simple Read Data from Standalone Calirbation/Monkey Fixaiton Task
%draft written by seth Konig 10/30/18

clar
%log_dir = 'Z:\MonkeyData\HumanFixationTaskData\';
%log_dir = 'Z:\MonkeyData\';
%log_dir = 'C:\Users\Wolmesdorf-Lab\Documents\monkeyfixation\logs\';
log_dir = 'Z:\MonkeyData\Wotan\Fixation\';
%log_dir = 'Z:\MonkeyData\Frey\';

%sess_file_name = 'Fixation__Frey__2018_11_05__15_55_13';
%sess_file_name = 'Fixation__Wotan__2018_11_05__15_42_31';
%sess_file_name = 'Fixation__SethAuto2__2018_11_05__15_22_27';
%sess_file_name = 'Fixation__ShelbyAuto__2018_11_05__15_14_39';

%Smaller 1.5 dva targets + 6 dva fixation windows
%sess_file_name = 'Fixation__Wotan__2018_11_27__15_06_25';
%sess_file_name = 'Fixation__Seth__2018_11_27__14_46_25';
% sess_file_name = 'Fixation__Frey__2018_12_14__12_55_52';

%Final Task version--training
%sess_file_name = 'Fixation__Wotan__2019_01_29__15_11_28';%good calibration, more trials, more variabel fixdurs
%sess_file_name = 'Fixation__Wotan__2019_01_29__15_06_25';%better calibration fewer trials

sess_file_name = 'Fixation__Wotan__2019_02_12__11_57_01';

trial_data_name = [log_dir sess_file_name '\RuntimeData\' sess_file_name '__TrialData.txt'];
frame_data_dir = [log_dir sess_file_name '\RuntimeData\FrameData\' sess_file_name];
frame_data_name = '__FrameData__Trial_';

UDP_recv_data_dir = [log_dir sess_file_name '\RuntimeData\UnityUDPRecvData\' sess_file_name];
UDP_recv_name = '__UDPRecv__Trial_';

gaze_data_dir = [log_dir sess_file_name '\RuntimeData\PythonData\GazeData\' sess_file_name];
gaze_data_name = '__GazeData__Trial_';

%% Read in Trial Data
trial_data = readtable(trial_data_name);

num_trials = size(trial_data,1);

fixation_epochs_start = [trial_data.Epoch2_StartFrame trial_data.Epoch2_StartTimeAbsolute trial_data.Epoch2_StartTimeRelative];%%Shrink start
fixation_epochs_end = [trial_data.Epoch4_StartFrame trial_data.Epoch4_StartTimeAbsolute trial_data.Epoch4_StartTimeRelative];% feedback start
rewarded = strcmpi(trial_data.isRewarded,'true');
target_location = [trial_data.TargetX trial_data.TargetY];
trial_code = trial_data.TrialCode;
%% Remove Unrewarded/failed trials
target_location(~rewarded,:) = NaN;
trial_code(~rewarded) = NaN;
%% Read in Frame Data
all_epoch = cell(1,num_trials);
all_frame = cell(1,num_trials);
all_time = cell(1,num_trials);
all_system_time = cell(1,num_trials);
all_xy = cell(1,num_trials);
nt = 0;
for t = 0:num_trials-1
    try
        temp_data = readtable([frame_data_dir frame_data_name num2str(t) '.txt']);
        
        if isempty(temp_data)
            continue
        end
        all_epoch{t+1} = temp_data.TrialEpoch;
        all_frame{t+1} = temp_data.Frame;
        all_time{t+1} = temp_data.FrameStartUnity;
        all_system_time{t+1} = temp_data.FrameStartSystem;
        all_xy{t+1} = [temp_data.EyetrackerTimeStamp temp_data.EyePositionX temp_data.EyePositionY];
        nt = nt+1;
    catch
        continue
    end
end
num_trials = nt;
%% Read in UDP receive file ...want to know frame received and Eye tracker time stamps
all_UDP_recv_data = cell(1,num_trials);
for t = 0:num_trials-1
    all_UDP_recv_data{t+1} = ImportUDPRecv([UDP_recv_data_dir UDP_recv_name num2str(t) '.txt']);
end
%% Read in Eye tracking data saved by Python at 600 Hz
all_pythonEye_data_time = cell(1,num_trials);
all_pythonEye_data_XY = cell(1,num_trials);
percent_binocular = NaN(1,num_trials);
average_participant_distance = NaN(1,num_trials);
for t = 0:num_trials-1
    try
        temp_data = readtable([gaze_data_dir gaze_data_name num2str(t) '.txt']);
    catch
        disp('File Missing')
        continue
    end
    
    
    all_pythonEye_data_XY{t+1} = [temp_data.left_ADCS_validity  temp_data.right_ADCS_validity  ...
        temp_data.left_ADCS_x temp_data.left_ADCS_y ...
        temp_data.right_ADCS_x temp_data.right_ADCS_y];
    
    try
        percent_binocular(t+1) = sum(~isnan(temp_data.left_ADCS_x) &  ~isnan(temp_data.right_ADCS_x))/...
            sum(~isnan(temp_data.left_ADCS_x) |  ~isnan(temp_data.right_ADCS_x));
    catch
        disp('Cant get participant eye data')
    end
    all_pythonEye_data_time{t+1} = [temp_data.system_time_stamp temp_data.device_time_stamp];
    try
        average_participant_distance(t+1) = nanmean(temp_data.left_origin_UCS_z+temp_data.right_origin_UCS_z)/2;
    catch
        disp('Cant get participant distance')
    end
end
%% Determine conversion from ADCS/pixels to DVA
average_eye_distance = nanmean(average_participant_distance)/10;%cm
full_screen_dva_x= atand(43.5/average_eye_distance)*2;
full_screen_dva_y = atand(24/average_eye_distance)*2;
%% Align Eye-Data Python-Eyetracker to Frames/Epochs

unique_trial_codes = unique(trial_code);
unique_trial_codes(isnan(unique_trial_codes)) = [];
%RMS_noise_Python = NaN(length(unique_trial_codes),4);

PythonRow_index_2_Fixation_Epochs = NaN(num_trials,2);
mean_eye_location_during_fixation_Unity = NaN(num_trials,2);
mean_eye_location_during_fixation_Python = NaN(num_trials,4);
for t = 1:num_trials-1
    
    fixation_frame_start = fixation_epochs_start(t,1);
    fixation_frame_start_time_Unity = fixation_epochs_start(t,2);
    
    fixation_frame_end = fixation_epochs_end(t,1);
    fixation_frame_end_time_Unity = fixation_epochs_end(t,2);
    
    frame_frame_start_index = find(all_frame{t} == fixation_frame_start);
    frame_frame_end_index = find(all_frame{t} == fixation_frame_end );
    
    if isempty(all_UDP_recv_data{t})
        continue
    end
    UDP_start_index = find(all_UDP_recv_data{t}(:,1) == fixation_frame_start);
    UDP_end_index = find(all_UDP_recv_data{t}(:,1) == fixation_frame_end);
    if length(UDP_end_index) > 1
        UDP_end_index = UDP_end_index(1);
        disp('Found Multiple UDP End Messages')
    end
    
    %verify frame start time is within 1 frame across files
    if abs(all_UDP_recv_data{t}(UDP_start_index,2)- fixation_frame_start_time_Unity) > 0.017
        disp('Fixation Epochs Start: UDP and Frame Start times are off :(')
    end
    if abs(all_UDP_recv_data{t}(UDP_end_index,2)- fixation_frame_end_time_Unity) > 0.017
        disp('Fixation Epochs Start: UDP and Frame Start times are off :(')
    end
    
    Eye_tracker_time_stamp_fixation_start = all_UDP_recv_data{t}(UDP_start_index,5);
    if isnan(Eye_tracker_time_stamp_fixation_start)
        disp('What')
    end
    
    Eye_tracker_time_stamp_fixation_end = all_UDP_recv_data{t}(UDP_end_index,5);
    if isnan(Eye_tracker_time_stamp_fixation_end)
        disp('What')
    end
    
    try
        python_ind  = find( all_pythonEye_data_time{t}(:,2) >= Eye_tracker_time_stamp_fixation_start & ...
            all_pythonEye_data_time{t}(:,2) <= Eye_tracker_time_stamp_fixation_end);
        
        PythonRow_index_2_Fixation_Epochs(t,1) = Eye_tracker_time_stamp_fixation_start;
        PythonRow_index_2_Fixation_Epochs(t,2) = Eye_tracker_time_stamp_fixation_start;
        
        mean_eye_location_during_fixation_Unity(t,1) = nanmean(all_xy{t}(frame_frame_start_index:frame_frame_end_index,2))/1920;
        mean_eye_location_during_fixation_Unity(t,2) = nanmean(all_xy{t}(frame_frame_start_index:frame_frame_end_index,3))/1080;
        
        mean_eye_location_during_fixation_Python(t,1) = nanmean(all_pythonEye_data_XY{t}(python_ind,3));
        mean_eye_location_during_fixation_Python(t,2) = nanmean(all_pythonEye_data_XY{t}(python_ind,4));
        mean_eye_location_during_fixation_Python(t,3) = nanmean(all_pythonEye_data_XY{t}(python_ind,5));
        mean_eye_location_during_fixation_Python(t,4) = nanmean(all_pythonEye_data_XY{t}(python_ind,6));
        
        %     RMS_noise_Python(t,1) = full_screen_dva_x*nanstd(all_pythonEye_data_XY{t}(python_ind,3));
        %     RMS_noise_Python(t,2) = full_screen_dva_y*nanstd(all_pythonEye_data_XY{t}(python_ind,4));
        %     RMS_noise_Python(t,3) = full_screen_dva_x*nanstd(all_pythonEye_data_XY{t}(python_ind,5));
        %     RMS_noise_Python(t,4) = full_screen_dva_y*nanstd(all_pythonEye_data_XY{t}(python_ind,6));
        
        %     UnityTime =  all_system_time{t};
        %     UnityTime = UnityTime-UnityTime(1);
        %     UnityTime = UnityTime/10^4;
        %
        %     PythonTime = all_pythonEye_data_time{t}(:,1);
        %     PythonTime = PythonTime-PythonTime(1);
        %     PythonTime =  PythonTime/10^2;
        %
        %
        %     figure
        %     subplot(2,1,1)
        %     plot(UnityTime,all_xy{t}(:,2)/1920)
        %     hold on
        %     plot(UnityTime,all_xy{t}(:,3)/1080)
        %     plot(UnityTime(frame_frame_start_index:frame_frame_end_index),all_xy{t}(frame_frame_start_index:frame_frame_end_index,2)/1920,'r')
        %     plot(UnityTime(frame_frame_start_index:frame_frame_end_index),all_xy{t}(frame_frame_start_index:frame_frame_end_index,3)/1080,'g')
        %     plot(UnityTime,ones(1,length(UnityTime))*target_location(t,1)/1920,'r--')
        %     plot(UnityTime,ones(1,length(UnityTime))*target_location(t,2)/1080,'g--')
        %     hold off
        %     box off
        %     xlabel('Time (ms)')
        %     ylabel('Eye Position')
        %     legend('Unity X','Unity Y')
        %     ylim([-0.25 1.5])
        %
        %     subplot(2,1,2)
        %     hold on
        %     plot(PythonTime,all_pythonEye_data_XY{t}(:,3));
        %     plot(PythonTime,1-all_pythonEye_data_XY{t}(:,4));
        %     plot(PythonTime,all_pythonEye_data_XY{t}(:,5));
        %     plot(PythonTime,1-all_pythonEye_data_XY{t}(:,6));
        %
        %      plot(PythonTime(python_ind),all_pythonEye_data_XY{t}(python_ind,3),'r');
        %     plot(PythonTime(python_ind),1-all_pythonEye_data_XY{t}(python_ind,4),'g');
        %     plot(PythonTime(python_ind),all_pythonEye_data_XY{t}(python_ind,5),'m');
        %     plot(PythonTime(python_ind),1-all_pythonEye_data_XY{t}(python_ind,6),'c');
        %
        %     plot(PythonTime,ones(1,length(PythonTime))*target_location(t,1)/1920,'r--')
        %     plot(PythonTime,ones(1,length(PythonTime))*target_location(t,2)/1080,'g--')
        %     hold off
        %     box off
        %     xlabel('Time (ms)')
        %     ylabel('Eye Position')
        %     legend('Python LX','Python LY','Python RX','Python RY')
        %     ylim([-0.25 1.25])
    end
end
%%
num_points = NaN(1,length(unique_trial_codes));
average_std_Unity = NaN(length(unique_trial_codes),2);
average_std_Python = NaN(length(unique_trial_codes),4);
accuracy_Unity = NaN(length(unique_trial_codes),2);
accuracy_Python = NaN(length(unique_trial_codes),4);
for utc = 1:length(unique_trial_codes)
    ind = find(trial_code == unique_trial_codes(utc));
    ind(ind > num_trials) = [];
    num_points(utc) = length(ind);
    
    accuracy_Unity(utc,1) = target_location(ind(1),1)/1920-nanmean(mean_eye_location_during_fixation_Unity(ind,1));
    accuracy_Unity(utc,2) = target_location(ind(1),2)/1080-nanmean(mean_eye_location_during_fixation_Unity(ind,2));
    
    accuracy_Python(utc,1) = target_location(ind(1),1)/1920-nanmean(mean_eye_location_during_fixation_Python(ind,1));
    accuracy_Python(utc,2) = (1-target_location(ind(1),2)/1080)-nanmean(mean_eye_location_during_fixation_Python(ind,2));
    accuracy_Python(utc,3) = target_location(ind(1),1)/1920-nanmean(mean_eye_location_during_fixation_Python(ind,3));
    accuracy_Python(utc,4) = (1-target_location(ind(1),2)/1080)-nanmean(mean_eye_location_during_fixation_Python(ind,4));
    
    average_std_Unity(utc,1) = nanstd(mean_eye_location_during_fixation_Unity(ind,1));
    average_std_Unity(utc,2) = nanstd(mean_eye_location_during_fixation_Unity(ind,2));
    
    average_std_Python(utc,1) = nanstd(mean_eye_location_during_fixation_Python(ind,1));
    average_std_Python(utc,2) = nanstd(mean_eye_location_during_fixation_Python(ind,2));
    
    average_std_Python(utc,3) = nanstd(mean_eye_location_during_fixation_Python(ind,3));
    average_std_Python(utc,4) = nanstd(mean_eye_location_during_fixation_Python(ind,4));
end
Unity_avg_error = NaN(1,2);
Unity_avg_error(1) = nanmean(average_std_Unity(num_points >= 4,1));
Unity_avg_error(2) = nanmean(average_std_Unity(num_points >= 4,2));

Python_avg_error = NaN(1,2);
Python_avg_error(1) = (nanmean(average_std_Python(num_points >= 4,1)) + nanmean(average_std_Python(num_points >= 4,3)))/2;
Python_avg_error(2) = (nanmean(average_std_Python(num_points >= 4,2)) + nanmean(average_std_Python(num_points >= 4,4)))/2;

Unity_mean_accuracy = NaN(1,2);
Unity_mean_accuracy(1) = sqrt(nansum(accuracy_Unity(:,1).^2))/sum(~isnan(accuracy_Unity(:,1)));
Unity_mean_accuracy(2) = sqrt(nansum(accuracy_Unity(:,2).^2))/sum(~isnan(accuracy_Unity(:,2)));

Python_mean_accuracy = NaN(1,4);
Python_mean_accuracy(1) = sqrt(nansum(accuracy_Python(:,1).^2))/sum(~isnan(accuracy_Python(:,1)));
Python_mean_accuracy(2) = sqrt(nansum(accuracy_Python(:,2).^2))/sum(~isnan(accuracy_Python(:,2)));
Python_mean_accuracy(3) = sqrt(nansum(accuracy_Python(:,3).^2))/sum(~isnan(accuracy_Python(:,3)));
Python_mean_accuracy(4) = sqrt(nansum(accuracy_Python(:,4).^2))/sum(~isnan(accuracy_Python(:,4)));
fixationstatsL = cell(1,num_trials-1);
fixationstatsR = cell(1,num_trials-1);
for t = 1:num_trials-1
    if ~isempty(all_pythonEye_data_XY{t})
        lx = all_pythonEye_data_XY{t}(:,3);
        ly = all_pythonEye_data_XY{t}(:,4);
        
        rx = all_pythonEye_data_XY{t}(:,5);
        ry = all_pythonEye_data_XY{t}(:,6);
        
        fixationstatsL{t} = ClusterFix_TobiiTX600([lx';ly']);
        fixationstatsR{t} = ClusterFix_TobiiTX600([rx';ry']);
        %         %%
        %         figure
        %         plot(lx)
        %         hold on
        %         plot(ly)
        %         for f = 1:size(fixationstatsL{t}.fixationtimes,2)
        %             plot(fixationstatsL{t}.fixationtimes(1,f):fixationstatsL{t}.fixationtimes(2,f),...
        %                 lx(fixationstatsL{t}.fixationtimes(1,f):fixationstatsL{t}.fixationtimes(2,f)),'k')
        %             plot(fixationstatsL{t}.fixationtimes(1,f):fixationstatsL{t}.fixationtimes(2,f),...
        %                 ly(fixationstatsL{t}.fixationtimes(1,f):fixationstatsL{t}.fixationtimes(2,f)),'k')
        %         end
        %         hold off
        %         xlabel('Sample #')
        %         ylabel('Eye Position')
        %         box off
        %         legend('X','Y','Fixations')
        %
        %         %%
    end
end
%% Determine RMS
RMS_noise_Python = NaN(500,4);
fixation_count = ones(1,2);
for t = 1:num_trials-1
    if ~isempty(fixationstatsL{t})
        
        lx = all_pythonEye_data_XY{t}(:,3);
        ly = all_pythonEye_data_XY{t}(:,4);
        for f = 1:size(fixationstatsL{t}.fixationtimes,2)
            RMS_noise_Python(fixation_count(1),1) = std(lx(fixationstatsL{t}.fixationtimes(1,f)+10:fixationstatsL{t}.fixationtimes(2,f)-10));
            RMS_noise_Python(fixation_count(1),2) = std(ly(fixationstatsL{t}.fixationtimes(1,f)+10:fixationstatsL{t}.fixationtimes(2,f))-10);
            
            %             if  RMS_noise_Python(fixation_count(1),1) > 0.01 ||  RMS_noise_Python(fixation_count(1),2) > 0.01
            %                 disp('Now')
            %             end
            %
            fixation_count(1) = fixation_count(1)+1;
            
            
        end
        
        rx = all_pythonEye_data_XY{t}(:,5);
        ry = all_pythonEye_data_XY{t}(:,6);
        for f = 1:size(fixationstatsR{t}.fixationtimes,2)
            RMS_noise_Python(fixation_count(2),3) = std(rx(fixationstatsR{t}.fixationtimes(1,f)+10:fixationstatsR{t}.fixationtimes(2,f))-10);
            RMS_noise_Python(fixation_count(2),4) = std(ry(fixationstatsR{t}.fixationtimes(1,f)+10:fixationstatsR{t}.fixationtimes(2,f))-10);
            
            %             if  RMS_noise_Python(fixation_count(1),3) > 0.01 ||  RMS_noise_Python(fixation_count(1),4) > 0.01
            %                 disp('Now')
            %             end
            fixation_count(2) = fixation_count(2)+1;
            
            
        end
    end
end
RMS_noise_Python(:,1) = RMS_noise_Python(:,1)*full_screen_dva_x;
RMS_noise_Python(:,3) = RMS_noise_Python(:,3)*full_screen_dva_x;

RMS_noise_Python(:,2) = RMS_noise_Python(:,2)*full_screen_dva_y;
RMS_noise_Python(:,4) = RMS_noise_Python(:,4)*full_screen_dva_y;

%%
clrs = 'rgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmc';


figure
% subplot(2,2,1)
% hold on
% for utc = 1:length(unique_trial_codes)
%     ind = find(trial_code == unique_trial_codes(utc));
%     if length(ind) < 3
%         continue
%     end
%     plot(mean_eye_location_during_fixation_Unity(ind,1),mean_eye_location_during_fixation_Unity(ind,2),[clrs(utc) '.'])
%     plot(nanmean(mean_eye_location_during_fixation_Unity(ind,1)),nanmean(mean_eye_location_during_fixation_Unity(ind,2)),[clrs(utc) '+'],'markersize',7)
%     plot(target_location(ind(1),1)/1920,target_location(ind(1),2)/1080,[clrs(utc) '*'],'markersize',10)
% end
% box off
% title(sprintf(['Unity ADCS \n' ...
%     'Average Consistency(std)_X = ' num2str(Unity_avg_error(1)*full_screen_dva_x,2) ...
%     ' dva, Average Consistency(std)_Y = ' num2str(Unity_avg_error(2)*full_screen_dva_y,2) ' dva \n' ...
%     'Accuracy_X = ' num2str(Unity_mean_accuracy(1)*full_screen_dva_x,2) ' dva, ' ...
%     'Accuracy_Y = ' num2str(Unity_mean_accuracy(2)*full_screen_dva_x,2) ' dva, ' ...
%     ]))
% xlim([0 1])
% ylim([0 1])
figure
subplot(2,2,3)
hold on
for utc = 1:length(unique_trial_codes)
    ind = find(trial_code == unique_trial_codes(utc));
    ind(ind > num_trials) = [];
    if length(ind) < 3
        continue
    end
    means = NaN(2,length(ind));
    for i = 1:length(ind)
        if isnan(mean_eye_location_during_fixation_Python(ind(i),1))
            plot(mean_eye_location_during_fixation_Python(ind(i),3),1-mean_eye_location_during_fixation_Python(ind(i),4),[clrs(utc) '.'])
            means(1,i) = mean_eye_location_during_fixation_Python(ind(i),3);
            means(2,i) = 1-mean_eye_location_during_fixation_Python(ind(i),4);
        else
            plot(mean_eye_location_during_fixation_Python(ind(i),1),1-mean_eye_location_during_fixation_Python(ind(i),2),[clrs(utc) '.'])
            means(1,i) = mean_eye_location_during_fixation_Python(ind(i),1);
            means(2,i) = 1-mean_eye_location_during_fixation_Python(ind(i),2);
        end
        
    end
    plot(target_location(ind(1),1)/1920,target_location(ind(1),2)/1080,[clrs(utc) '*'],'markersize',10)
    plot(mean(means(1,:)),mean(means(2,:)),[clrs(utc) '+'],'markersize',7)
    
    
end
box off
title(sprintf(['Python ADCS \n ' ...
    'Average Consistency(std)_X = ' num2str(Python_avg_error(1)*full_screen_dva_x,2) ...
    ' dva, Average Consistency(std)_Y = ' num2str(Python_avg_error(2)*full_screen_dva_y,2) ' dva \n' ...
    'Accuracy_LX = ' num2str(Python_mean_accuracy(1)*full_screen_dva_x,2) ' dva, ' ...
    'Accuracy_LY = ' num2str(Python_mean_accuracy(2)*full_screen_dva_x,2) ' dva, ' ...
    'Accuracy_RX = ' num2str(Python_mean_accuracy(3)*full_screen_dva_x,2) ' dva, ' ...
    'Accuracy_RY = ' num2str(Python_mean_accuracy(4)*full_screen_dva_x,2) ' dva, ' ...
    ] ))
xlim([0 1])
ylim([0 1])

subplot(2,2,[1 2])
t = 14;
PythonTime = all_pythonEye_data_time{t}(:,1);
PythonTime = PythonTime-PythonTime(1);
PythonTime =  PythonTime/10^2;
hold on
plot(PythonTime,all_pythonEye_data_XY{t}(:,3));
plot(PythonTime,1-all_pythonEye_data_XY{t}(:,4));
plot(PythonTime,all_pythonEye_data_XY{t}(:,5));
plot(PythonTime,1-all_pythonEye_data_XY{t}(:,6));
plot(PythonTime,ones(1,length(PythonTime))*target_location(t,1)/1920,'r--')
plot(PythonTime,ones(1,length(PythonTime))*target_location(t,2)/1080,'g--')
hold off
box off
xlabel('Time (ms)')
ylabel('Eye Position')
legend('Python LX','Python LY','Python RX','Python RY','Target X','Target Y')
ylim([-0.25 1.25])
title('Example Python Eye Trace')

rnp = RMS_noise_Python(:);
rnp(isnan(rnp)) = [];
rnp(rnp > prctile(RMS_noise_Python(:),95)) = [];
subplot(2,2,4)
hist(rnp,25)
box off
title(sprintf(['RMS Noise in Python Data during all Fixation(s) \n' ...
    'LX = ' num2str(nanmean(RMS_noise_Python(:,1)),2) ' dva, LY = ' num2str(nanmean(RMS_noise_Python(:,2)),2) ' dva \n' ...
    'RX = ' num2str(nanmean(RMS_noise_Python(:,3)),2) ' dva, RY = ' num2str(nanmean(RMS_noise_Python(:,4)),2) ' dva \n' ...
    ]))

subtitle(['Average Eye Position by Calibration Point: ' sess_file_name])
%%
all_unityEye_time = [];
for t=1:length(all_xy)
    if ~isempty( all_xy{t})
    all_unityEye_time = [all_unityEye_time;  all_xy{t}(:,1)];
    end
end
plot(diff(all_unityEye_time)/10^6)