%%http://developer.tobiipro.com/commonconcepts/gaze.html
dr = 'C:\Users\Wolmesdorf-Lab\Desktop\EyeTrackingBridge\logs\Fixation__sub0__2018_10_29__20_02_31\PythonData\GazeData\';


file_base_name = 'Fixation__sub0__2018_10_29__20_02_31';
gaze_data_base = '__GazeData__Trial_';

num_trials = 6;
for t = 2%:num_trials
   data = readtable([dr file_base_name gaze_data_base num2str(t) '.txt']);
   
    
end
%%
figure
plot(data.left_gaze_point_on_display_area_x)
hold on
plot(data.left_gaze_point_on_display_area_y)
title('ADCS (Active Display Coordinate System) (normalized)')
legend('X','Y')
%%
figure
plot(data.left_gaze_origin_in_user_coordinate_system_x)
hold on
plot(data.left_gaze_origin_in_user_coordinate_system_y)
plot(data.left_gaze_origin_in_user_coordinate_system_z)
plot(data.right_gaze_origin_in_user_coordinate_system_x)
plot(data.right_gaze_origin_in_user_coordinate_system_y)
plot(data.right_gaze_origin_in_user_coordinate_system_z)
title('Origin in UCS (User Coordinate System) (mm)')
legend('LX','LY','LZ','RX','RY','RZ')

%%
figure
plot(data.left_gaze_point_in_user_coordinate_system_x)
hold on
plot(data.left_gaze_point_in_user_coordinate_system_y)
plot(data.left_gaze_point_in_user_coordinate_system_z)
title('UCS (User Coordinate System) (mm)')
legend('X','Y','Z')
%%
figure
plot(data.left_gaze_origin_in_trackbox_coordinate_system_x)
hold on
plot(data.left_gaze_origin_in_trackbox_coordinate_system_y)
plot(data.left_gaze_origin_in_trackbox_coordinate_system_z)
title('TBCS (TrackBox Coordinate System) (normalized)')
legend('X','Y','Z')

%%
figure
plot(data.left_gaze_point_on_display_area_x*1920)
hold on
plot(data.left_gaze_point_on_display_area_y*1080)
plot(data.left_gaze_point_in_user_coordinate_system_x/435*1920+950)
plot(1080-data.left_gaze_point_in_user_coordinate_system_y/240*1080+125)
title('Pixel Space')
legend('ADCS_X','ADCS_Y','UCS_X','UCS_Y')